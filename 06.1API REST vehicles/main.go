package main

import (
	"encoding/json"
	"fmt"
	"log"

	// "gorilla/mux"
	"net/http"

	"github.com/gorilla/mux"
)

type cylinderCapacity struct {
	Engine string `json:"Engine"`
	Power  string `json:"Power"`
	Torque string `json:"Torque"`
}

type reference struct {
	// ReferenceId string ...
	Model            string           `json:"Model"`
	CylinderCapacity cylinderCapacity `json:"cylinder_capacity"`
}

type car struct {
	Id         int         `json:"Id"`
	Brand      string      `json:"Brand"`
	References []reference `json:"References"`
}

type motorcycle struct {
	Id         int         `json:"Id"`
	Brand      string      `json:"Brand"`
	References []reference `json:"References"`
}

type vehicle struct {
	Cars        []car        `json:"Cars"`
	Motorcycles []motorcycle `json:"Motorcicles"`
}

var vehicles = vehicle{
	Cars: []car{
		{
			Id:    1,
			Brand: "chevrolet",
			References: []reference{
				{
					Model: "Joy Sedan",
					CylinderCapacity: cylinderCapacity{
						Engine: "1.4L",
						Power:  "97HP",
						Torque: "126NM",
					},
				},
				{
					Model: "Onix Turbo Sedan",
					CylinderCapacity: cylinderCapacity{
						Engine: "1.4L",
						Power:  "99HP",
						Torque: "146NM",
					},
				},
				{
					Model: "Spark",
					CylinderCapacity: cylinderCapacity{
						Engine: "1.2L",
						Power:  "85HP",
						Torque: "110NM",
					},
				},
			},
		},
	},
	Motorcycles: []motorcycle{
		{
			Id:    1,
			Brand: "susuki",
			References: []reference{
				{
					Model: "gsx 150",
					CylinderCapacity: cylinderCapacity{
						Engine: "1L",
						Power:  "14HP",
						Torque: "16NM",
					},
				},
				{
					Model: "gixxer",
					CylinderCapacity: cylinderCapacity{
						Engine: "1L",
						Power:  "12HP",
						Torque: "15NM",
					},
				},
				{
					Model: "v-strom 250",
					CylinderCapacity: cylinderCapacity{
						Engine: "248cc",
						Power:  "25HP",
						Torque: "23NM",
					},
				},
			},
		},
	},
}

func main() {
	fmt.Println("h1")

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", indexPage)
	router.HandleFunc("/vehicles/", getVehicles)
	router.HandleFunc("/vehicles/cars/", getCars)
	router.HandleFunc("/vehicles/motorcycles/", getMotorcycles)

	log.Fatal(http.ListenAndServe(":3030", router))
}

func indexPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "WELCOME TO MY DIRECTORY")
}

func getVehicles(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(vehicles)
}

// gingonic / arq clean reciber funcs
func getCars(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(vehicles.Cars)
}

func getMotorcycles(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(vehicles.Motorcycles)
}
