package main

import "fmt"

func main() {
	fmt.Println("--------------------------CLOUSURES---------------------------")
	c1 := clousure()
	fmt.Println("C1\t", c1())
	fmt.Println("C1\t", c1())
	fmt.Println("C1\t", c1())

	c2 := clousure()
	fmt.Println("c2\t", c2())
	fmt.Println("c2\t", c2())
	fmt.Println("c2\t", c2())

	fmt.Println("C1\t", c1())
	fmt.Println("C1\t", c1())

	fmt.Println(clousure()())
	fmt.Println(clousure()())
}

func clousure() func() int {
	x := 0
	return func() int {
		x++
		return x
	}
}
