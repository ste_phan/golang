package main

import (
	"fmt"
	"math"
)

// there we can see how to create a metohd for a own type

type person struct {
	name     string
	nickname string
	old      int
}

func (p person) present() string {
	return fmt.Sprintln("hi, my name is", p.name, p.nickname, " and i'm", p.old)
}

// now i am going to use interface
type square struct {
	length float64
}

type circle struct {
	radius float64
}

func (s square) area() float64 {
	return s.length * s.length
}

func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

type form interface {
	area() float64
}

func info(f form) float64 {
	return f.area()
}

func main() {
	p1 := person{
		name:     "Stephan",
		nickname: "Suarez",
		old:      21,
	}
	fmt.Println(p1.present())

	// now i am going to use interface
	sq := square{
		length: 3.4,
	}

	cir := circle{
		radius: 3.4,
	}

	// fmt.Println(sq.area())
	// fmt.Println(cir.area())
	fmt.Println(info(sq))
	fmt.Println(info(cir))
}
