package main

import (
	"fmt"
)

type person struct {
	Name     string
	Gretting string
}

func (p *person) speak() string {
	return fmt.Sprintln("De: ", p.Name, "\nSaludo: ", p.Gretting)
}

type human interface {
	speak() string
}

func sayHello(h human) {
	fmt.Println(h.speak())
}

func main() {
	per := person{
		Name:     "stephan",
		Gretting: "Good morning",
	}

	fmt.Println(per.speak())
	sayHello(&per)
}
