package main

import "fmt"

type person struct {
	name string
	old  int
}

func p(p *person) {
	//p.name = "j"
	(*p).name = "jjjj"
	fmt.Println("aaaaaa")
}

func main() {
	p1 := person{
		name: "stephan",
		old:  21,
	}
	fmt.Println(p1)
	p(&p1)
	fmt.Println(p1)

}
