package main

import "fmt"

// A pointer in go is a variable that allows us to access a memory space of another variable.

func main() {
	a := 2
	fmt.Println(a)
	fmt.Println(&a)

	b := &a
	fmt.Println(b)
	fmt.Printf("type = %T", b)
	fmt.Println(&b)
	fmt.Printf("type = %T", b)

	fmt.Println("-------------------------------------------------------------")
	c := &b
	fmt.Println(c)
	fmt.Printf("type = %T", c)
	fmt.Println(&c)
	fmt.Printf("type = %T", c)

}
