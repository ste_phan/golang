package main

import "fmt"

var function func() = func() {
	fmt.Println("Declaring a variable of type function and assigning it a function")
}

func main() {
	function()

	defer fmt.Println(deferShow())

	fmt.Println("-----------------------FUNCTIONS----------------------------")
	fmt.Println(onlyOneReturn(1, 2))
	a, b := towReturn(1, 2)
	fmt.Println(b, a)
	// fmt.Println(towReturn(1, 2))

	fmt.Println("--------------------FUNCTIONS SEVERAL PARAMS----------------------")
	sliceEj := []int{1, 2, 3, 4, 5}
	// fmt.Println(severalParams(sliceEj)) // if in the params of func ins []type
	fmt.Println(severalParams(sliceEj...))

	fmt.Println("-------------Function that return another function-------------------")
	fmt.Println(returnFunc()())
}

func onlyOneReturn(a, b int) int {
	return a + b
}

func towReturn(a, b int) (string, int) {
	return "the result", a + b
}

func severalParams(x ...int) int {
	sum := 0
	for v := range x {
		sum += v
	}
	return sum
}

func deferShow() string {
	defer func(msg string) {
		fmt.Println(msg)
	}("--------------------FUNCTIONS DEFER----------------------\n============")
	fmt.Println("============")
	return "this is the final message"
}

// function that return another function
func returnFunc() func() int {
	return func() int {
		return 32
	}
}
