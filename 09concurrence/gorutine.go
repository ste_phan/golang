package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("CPUs: ", runtime.NumCPU())
	fmt.Println("Gorutines: ", runtime.NumGoroutine())

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		fmt.Println("First Gorutine......")
		wg.Done()
	}()
	go func() {
		fmt.Println("Second Gorutine.....")
		wg.Done()
	}()

	fmt.Println("CPUs: ", runtime.NumCPU())
	fmt.Println("Gorutines: ", runtime.NumGoroutine())
	wg.Wait()
	fmt.Println("CPUs: ", runtime.NumCPU())
	fmt.Println("Gorutines: ", runtime.NumGoroutine())
}
