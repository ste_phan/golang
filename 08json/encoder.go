package main

import (
	"encoding/json"
	"os"
)

type Person struct {
	Name  string
	Old   int
	Foods []string
}

func main() {
	// var persons = []Person{
	persons := []Person{
		{
			Name:  "stephan",
			Old:   31,
			Foods: []string{"meat", "chicken"},
		},
		{
			Name:  "stephan2",
			Old:   312,
			Foods: []string{"meat2", "chicken2"},
		},
	}

	en := json.NewEncoder(os.Stdout) //.Encode(persons)
	en.Encode(persons)

}
