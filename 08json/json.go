package main

// you can see how to convert json to go

import (
	"encoding/json"
	"fmt"
)

// need to have the struct of the json in go  USE UPPER please
type Person struct {
	Name string
	Old  int
}

func main() {
	// first we get a json
	s := `[{"Name": "Stephan", "Old": 13},{"Name": "Stephan", "Old": 13}]`
	// it's necessary have this string in "byte slices'" format
	sliceByte := []byte(s)
	// use the sturc array ...
	var listPerson []Person
	// listPerson := []Person{}

	//You must call the function json.Unmarshal and pass as argument the "byte slice" and the address in memory where you want it to be saved, this returns an error (null or of some type)
	err := json.Unmarshal(sliceByte, &listPerson)
	if err != nil {
		fmt.Println("Error: ", err)
	}
	fmt.Println(listPerson)

}
