package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var list = []byte(`[
		{"Name": "Stephan", "Old": 13},
		{"Name": "Stephan", "Old": 13}
	]`)

	fmt.Printf("%T  =  ", string(list))
	fmt.Println(string(list), "\n--------------------------------------------------------")

	type Person struct {
		Name string
		Old  int
	}

	var persons []Person
	err := json.Unmarshal(list, &persons)
	if err != nil {
		fmt.Println("err: ", err)
	}
	fmt.Println(persons)
}
