package main

// now let's see how to convert a go object/type to a json string
import (
	"encoding/json"
	"fmt"
)

func main() {
	// Make the structure of the objects that are inside the json
	type colors struct {
		Id     string
		Name   string
		Colors []string
	}

	// listColors := colors{
	// 	Id:     "a1",
	// 	Name:   "red",
	// 	Colors: []string{"Ruby", "Red"},
	// }

	listColors := []colors{
		{
			Id:     "a1",
			Name:   "red",
			Colors: []string{"Ruby", "Red"},
		},
		{
			Id:     "b1",
			Name:   "blue",
			Colors: []string{"Sky Blue", "Lapis Lazuli"},
		},
	}

	fmt.Printf("-%T  =  ", listColors)
	fmt.Println(listColors)

	fmt.Println("---------------------------------")
	// call the marshall method and pass as argument the list or object it has, this function returns a uint8 segment and an error if it exists,
	listColorsJson, err := json.Marshal(listColors)
	if err != nil {
		fmt.Println("error:", err)
	}
	// tranform the uint8 to string
	fmt.Printf("%T  =  ", string(listColorsJson))
	fmt.Println(string(listColorsJson))
}
