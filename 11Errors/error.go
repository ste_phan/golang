package main

import (
	"encoding/json"
	"fmt"
)

type personError struct {
	Name string
	Old  int
}

func main() {
	p1 := personError{
		Name: "ste",
		Old:  12,
	}

	bs, err := toJSON(p1)
	if err != nil {
		fmt.Errorf("error: %v", err)
	}
	fmt.Println(bs)
}

func toJSON(p personError) ([]byte, error) {
	bs, err := json.Marshal(p)
	return bs, err
}
