package main

import (
	dog "D:\\desarrollo\\golang\\12Doc\\dog"
	"fmt"
)

type Dog struct {
	Name string
	Old  int
}

func main() {
	myDog := Dog{
		Name: "pepe",
		Old:  dog.DogOld(3),
	}
	fmt.Println(myDog)
}
