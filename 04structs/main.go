package main

import (
	"fmt"
)

type person struct {
	name, nickname  string
	iceCreamFlavors []string
}

type vehicle struct {
	doors int
	color string
}

type truck struct {
	vehicle
	fourwheels bool
}

type seda struct {
	vehicle
	luxurious bool
}

func main() {
	fmt.Println("------------------------STRUCTS-----------------------------")
	person1 := person{
		name:            "stephan",
		nickname:        "suarez",
		iceCreamFlavors: []string{"chocolate", "brawnie"},
	}
	fmt.Printf("Type %T\tValue %v\n", person1, person1)
	fmt.Println(person1.name, person1.nickname, person1.iceCreamFlavors[0])

	p1 := map[string]person{
		person1.nickname: person1,
	}
	fmt.Println(p1)

	for _, v := range p1 {
		fmt.Println(v.name)
	}

	fmt.Println("------------------------STRUCTS embedded-----------------------------")
	t1 := truck{
		vehicle: vehicle{
			doors: 4,
			color: "red",
		},
		fourwheels: true,
	}
	fmt.Println(t1, "\t\tcolor: ", t1.color)

	s1 := seda{
		vehicle: vehicle{
			doors: 6,
			color: "black",
		},
		luxurious: true,
	}
	fmt.Println(s1, "\tcolor: ", s1.color)

	fmt.Println("------------------------STRUCTS ANONIMUOS-----------------------------")
	p2 := struct {
		name    string
		friends map[string]person
	}{
		name: "ste",
		friends: map[string]person{
			"lola": {
				name:            "lola",
				nickname:        "cap",
				iceCreamFlavors: []string{"chocolate", "brawnie"},
			},
			"LOLO": {
				name:            "lolo",
				nickname:        "cap",
				iceCreamFlavors: []string{"Oranges"},
			},
		},
	}
	fmt.Println(p2)
	fmt.Println(p2.name, "has this friends")
	for k, v := range p2.friends {
		fmt.Println("-", k)
		fmt.Println("\t", v.name, v.nickname)
		for _, v := range v.iceCreamFlavors {
			fmt.Println("\t\t-", v)
		}
	}

}
