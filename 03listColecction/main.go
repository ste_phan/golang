// On this document you will find how to use collection list, like, arrays, slices, maps

package main

import (
	"fmt"
)

func main() {
	// arrays is a data type used to save a type of data (int string bool ...) this conllection is inmutable
	array := [5]int{1, 2, 3}
	fmt.Print(array)
	fmt.Printf("\t%T\n", array)
	fmt.Println(len(array), "\t", cap(array), "\nindex\tvalue")
	for i, v := range array {
		fmt.Println("-", i, "\t", v)
	}
	// slice is aonther type of data used to save collection data types, the difference witht arrays is there are mutables, their capacity can variable
	arr2 := array[:2]
	fmt.Printf("%T\n", arr2)
	arr2 = append(arr2, 1, 2, 3, 4, 5, 6, 7, 8, 9)
	fmt.Print(cap(arr2), arr2)
	slice := []int{1, 2, 3, 4, 5, 6, 7, 8}
	fmt.Println("\n", slice, "\t", len(slice), "\t", cap(slice))
	slice = append(slice, arr2...)
	fmt.Println("\n", slice, "\t", len(slice), "\t", cap(slice))
	// fmt.Println(slice[-1])

	slice2 := make([]string, 3, 3) // the diefference is that in this case you can specify the len and capacity of you slice
	fmt.Println(slice2, len(slice2), cap(slice2))
	slice2 = append(slice2, "1")
	fmt.Println(slice2[0])
	fmt.Println(slice2, len(slice2), cap(slice2))
	slice2 = []string{}
	fmt.Println(slice2, len(slice2), cap(slice2))
	slice2 = []string{"hi"}
	fmt.Println(slice2, len(slice2), cap(slice2))

	// create a slice of more slice or arrays inside
	s1 := []int{1, 2, 3}
	s2 := []int{1, 2, 3}
	s5 := []int{1, 2, 3, 4, 5}
	// s3 := []int{s1..., s2...}
	fmt.Println(s1, s2)
	s3 := [][]int{s1, s2}
	fmt.Println(s3)
	s3 = append(s3, s5)
	fmt.Println(s3)

	// maps are dictionaries in go, it contain two values associated, key and value, this value can be someone data type
	name := map[int]string{
		1: "stepnan",
	}
	fmt.Println(name)

	for k, v := range name {
		fmt.Println(k, v)
	}

	names := map[int][]string{
		1: {"stephan", "suarez", "32"},
		// 2: []string{"stephan1", "suarez2", "322"}, this is redundancy
		2: {"stephan1", "suarez2", "322"},
	}
	fmt.Println(names)

	for k, v := range names { // k = key, v = value
		fmt.Println(k, v)
		for i := range v { // i = index, v - valeu too
			fmt.Println("\t", i, "\t", names[k][i])
		}
	}

	// next you can see how to add a regists in the map
	names[3] = []string{"zarah", "suarez", "12"}
	for k, v := range names { // k = key, v = value
		fmt.Println(k, v)
		for i, v := range v { // i = index, v - valeu too
			fmt.Println("\t", i, "\t", v)
		}
	}

	// and this is used to delete a regist
	delete(names, 2)
	names[3] = []string{"zarah", "suarez", "12"}
	for k, v := range names { // k = key, v = value
		fmt.Println(k, v)
		for i, v := range v { // i = index, v - valeu too
			fmt.Println("\t", i, "\t", v)
		}
	}

	// also you can comprove if the resgist existo to delete
	if v, ok := names[1]; ok {
		fmt.Println(v)
	} else {
		fmt.Println("the value doesn't exist")
	}
}
