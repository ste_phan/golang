// on this document you will find how you can use loops, like for and conditionals like if else and switch with logical operators

package main

import "fmt"

func main() {
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
	i := 5
	for i < 10 {
		fmt.Println(i)
		i++
	}
	for {
		fmt.Println(i)
		i++
		if i == 15 {
			break
		}
	}

	fmt.Println("----------------------RUNE CODE POINT ASCII--------------------------")
	for i := 65; i <= 90; i++ {
		fmt.Println("Caracters ASCII", i)
		for j := 0; j < 3; j++ {
			fmt.Printf("\tLetra'%#U'\t\n", i)
		}
	}

	fmt.Println("----------------------IF ELSE--------------------------")
	num := 10
	if num <= 10 && num > 0 {
		fmt.Println(true)
	} else if num == 10 {
		fmt.Println(false)
	} else {
		fmt.Println(false)
	}

	fmt.Println("----------------------SWITCH--------------------------")
	switch {
	case true:
		fmt.Println(true)
	}
	switch "apple" {
	case "orange", "apple":
		fmt.Println(true)
		fallthrough // this keyword allows the following case to be executed, no matter if it is false
	default:
		fmt.Println(false)
	}

}
