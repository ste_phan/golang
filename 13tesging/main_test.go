package main

import (
	"testing"
)

func TestSum(t *testing.T) {
	// Arrange
	value1 := 3
	value2 := 5
	value3 := 9
	resultExpected := 17

	// Act
	result := sum(value1, value2, value3)

	// Assert
	if result != resultExpected {
		t.Logf("Expected: %v. Got %v", resultExpected, result)
		t.Error()
	}
}

func TestSumComplete(t *testing.T) {
	type sumNumber struct {
		name    string
		numbers []int
		result  int
	}

	testSums := []sumNumber{
		{
			name:    "sum Only positives",
			numbers: []int{1, 2, 3, 4, 5},
			result:  15,
		},
		{
			name:    "sum Only negatives",
			numbers: []int{-1, -2, -3, -4, -5},
			result:  -15,
		},
		{
			name:    "sun negatives and positives",
			numbers: []int{-1, -2, -3, 4, 5},
			result:  3,
		},
	}
	for _, test := range testSums {
		t.Run(test.name, func(t *testing.T) {
			// Act
			result := sum(test.numbers...)

			// Assert
			if result != test.result {
				t.Errorf("Test case '%s': Expected %d, Got %d", test.name, test.result, result)
			}
		})
	}
}
