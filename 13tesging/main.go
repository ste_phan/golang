package main

import "fmt"

func main() {
	fmt.Println("2+4=", sum(2, 4))
	fmt.Println("5+4=", sum(5, 4))
}

// sum adds all the numbers it has in its parameters
func sum(nums ...int) int {
	sumValue := 0
	for _, v := range nums {
		sumValue += v
	}
	return sumValue
}
